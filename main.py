import telebot

from telebot.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    ReplyKeyboardMarkup
)


TOKEN = "6882257505:AAFNAq4Ueu4Kf_PNlOdIKTTcYcvWI40h_L0"


bot = telebot.TeleBot(TOKEN)

users = {}

@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message, """\
Hi there, I am EchoBot.
I am here to echo your kind words back to you. Just say anything nice and I'll say the exact same thing to you!\
""")
    

@bot.message_handler(commands=['help'])
def help_message_handler(message):
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    keyboard.add('💸Прайс', '✅ В наличие', '🛍Магазин')
    keyboard.add('Как купить ⁉️ инструкция', '👉Пополнить btc/ltc')
    keyboard.add('👤Профиль/баланс', '🛎 Ненаход/Правила')
    keyboard.add('⭐️ ПРЕДЗАКАЗ ⭐️')
    bot.reply_to(message, "Вам надо идти налево", reply_markup=keyboard)

@bot.message_handler(commands=["save_me"])
def save_user_info(message):
    msg = bot.send_message(chat_id=message.chat.id, text="Отправьте свое имя")
    bot.register_next_step_handler(msg, save_name)

@bot.message_handler(commands=["who_i"])
def get_user_info(message):
    user_data = users[message.chat.id]
    bot.send_message(chat_id=message.chat.id, text=f"Ваше имя {user_data['name']} {user_data['surname']}")

@bot.message_handler(content_types=["text"])
def main_message_handler(message):
    if message.text == "💸Прайс":
        bot.send_message(chat_id=message.chat.id, text="Стоимость нашей услуги 10 000")
    elif message.text == "Как купить ⁉️ инструкция":
        bot.send_message(chat_id=message.chat.id, text="перейдите на ссылке https://www.youtube.com/")
    else:
        bot.send_message(chat_id=message.chat.id, text="Такой команды у нас в программе не имеется!")

def save_name(message):
    users[message.chat.id] = {
        "name" : message.text
        }
    msg = bot.send_message(message.chat.id, text="Отправьте нам свою фамилию")
    bot.register_next_step_handler(msg, save_surname)

def save_surname(message):
    users[message.chat.id]["surname"] = message.text
    bot.send_message(chat_id=message.chat.id, text="Ваши данные мы сохранили...")


bot.infinity_polling()